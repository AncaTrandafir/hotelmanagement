package ubb.AncaTrandafir.HotelManagement;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import java.util.Arrays;


@SpringBootApplication
@ComponentScan(basePackages={"ubb"})
@EnableJpaRepositories(basePackages = "ubb.core.repository")
@EntityScan(basePackages = {"ubb.hotelMngWeb.controller", "ubb.core"})
    //    (scanBasePackages = {"ubb.hotelMngWeb.controller", "core.service"})

public class HotelManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelManagementApplication.class, args);
	}


}
