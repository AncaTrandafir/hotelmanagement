package ubb.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    // create user
    @Autowired

    //Prior to Spring Security 5.0 the default PasswordEncoder was NoOpPasswordEncoder which required plain text passwords.
    // In Spring Security 5, the default is DelegatingPasswordEncoder, which required Password Storage Format.
    // Solution: Add password storage format, for plain text, add {noop}
    // Se salveaza gen {bcrypt}$2a$10$LoV/3z36G86x6Gn101aekuz3q9d7yfBp3jFn7dzNN/AL5630FyUQ, criptata, si eu vreau sa o salveze plain text => {noop}

    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()       // autentificare inMemory
                .withUser("admin").password("{noop}admin").roles("ADMIN", "USER")  // -> cu rol de admin
                .and().withUser("user").password("{noop}user").roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/login").permitAll()   // -> toti cei care acceseaza /login au acces la login, permitAll
                .antMatchers("/", "/*reservation*/**", "/*hotel*/**", "/*room*/**")     // -> toti cei care acceseaza orice care contine pattern, au rol de User si sunt directionati automat catre formularul de login
                .access("hasRole('USER')").and()
                .formLogin();   // -> genereaza el un loginForm
    }

}
