package ubb.core.service;

import ubb.core.model.Guest;
import ubb.core.model.Hotel;
import ubb.core.model.Reservation;
import ubb.core.model.Room;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public interface ReservationService {
    List<Reservation> findAll();

    Reservation updateReservation(Long id, Guest guest, Date arrivalDate, Date departureDate, Room room, Boolean breakfastIncluded);

    Reservation addReservation(Long id, Guest guest, Date arrivalDate, Date departureDate, Room room, Boolean breakfastIncluded);

    void delete(Long id);
}
