package ubb.core.service;

import org.springframework.beans.factory.annotation.Qualifier;
import ubb.core.model.Hotel;
import ubb.core.repository.HotelRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service("hotelService")
public class HotelServiceImpl implements HotelService {
    private static final Logger log = LoggerFactory.getLogger(HotelServiceImpl.class);

    @Autowired
    @Qualifier("hotelRepository")
    private HotelRepository hotelRepository;


    @Override
    public List<Hotel> findAll() { // Perisistence repo returneaza List la findAll
        List<Hotel> hotels = hotelRepository.findAll();

        return hotels;
    }


    @Override
    @Transactional
    public Hotel updateHotel(Long idHotel, String hotelName, String location) {     //, Set<Room> rooms) {
        Optional<Hotel> hotel = hotelRepository.findById(idHotel);

        hotel.ifPresent(s->{
            s.setIdHotel(idHotel);
            s.setHotelName(hotelName);
            s.setLocation(location);
          //  s.setRooms(rooms);
        });

        return hotel.orElse(null);
    }

    @Override
    public Hotel addHotel(Long idHotel, String hotelName, String location) {    //, Set<Room> rooms) {

        Hotel hotel = (Hotel) hotelRepository.save(new Hotel(idHotel, hotelName, location));    //, rooms));
        return hotel;
    }

    @Override
    public void delete(Long id) {
        hotelRepository.deleteById(id);
    }

    @Override
    public Optional<Hotel> findById(Long id) {
        return hotelRepository.findById(id);
    }

}
