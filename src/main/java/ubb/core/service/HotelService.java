package ubb.core.service;

import ubb.core.model.Hotel;
import java.util.List;
import java.util.Optional;

public interface HotelService {
    List<Hotel> findAll();

    Hotel updateHotel(Long id, String hotelName, String location);  //, Set<Room> rooms);

    Hotel addHotel(Long id, String hotelName, String location); //, Set<Room> rooms);

    void delete(Long id);

    Optional<Hotel> findById(Long id);
}
