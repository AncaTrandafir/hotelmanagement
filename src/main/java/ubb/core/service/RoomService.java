package ubb.core.service;

import ubb.core.model.Hotel;
import ubb.core.model.Reservation;
import ubb.core.model.Room;
import ubb.core.model.RoomType;

import java.util.List;
import java.util.Set;


public interface RoomService {
    List<Room> findAll();

    Room updateRoom(Long id, Hotel hotel, RoomType roomType, int beds, float costPerNight); //, Set<Reservation> reservations);

    Room addRoom(Long id, Hotel hotel, RoomType roomType, int beds, float costPerNight);    //, Set<Reservation> reservations);

    void delete(Long id);
}
