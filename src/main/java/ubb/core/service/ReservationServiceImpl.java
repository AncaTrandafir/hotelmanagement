package ubb.core.service;

import org.springframework.beans.factory.annotation.Qualifier;
import ubb.core.model.Guest;
import ubb.core.model.Reservation;
import ubb.core.model.Room;
import ubb.core.repository.ReservationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service("reservationService")
public class ReservationServiceImpl implements ReservationService {
    private static final Logger log = LoggerFactory.getLogger(ReservationServiceImpl.class);

    @Autowired
    @Qualifier("reservationRepository")
    private ReservationRepository reservationRepository;


    @Override
    public List<Reservation> findAll() {
        log.trace("findAll --- method entered");

        List<Reservation> reservations = reservationRepository.findAll();

        log.trace("findAll: reservations={}", reservations);

        return reservations;
    }

    @Override
    @Transactional
    public Reservation updateReservation(Long idReservation, Guest guest, Date arrivalDate, Date departureDate, Room room, Boolean breakfastIncluded) {
        log.trace("updateReservation: idReservation={}, guest={}, arrivalDate={}, departureDate={}, room={}, breakfastIncluded={}", idReservation, guest, arrivalDate, departureDate, room, breakfastIncluded);

        Optional<Reservation> reservation = reservationRepository.findById(idReservation);

        reservation.ifPresent(s->{
            s.setIdReservation(idReservation);
            s.setGuest(guest);
            s.setArrivalDate(arrivalDate);
            s.setDepartureDate(departureDate);
            s.setRoom(room);
            s.setBreakfastIncluded(breakfastIncluded);
        });

        log.trace("updateReservation: reservation={}", reservation.get());

        return reservation.orElse(null);
    }

    @Override
    public Reservation addReservation(Long idReservation, Guest guest, Date arrivalDate, Date departureDate, Room room, Boolean breakfastIncluded) {
        log.trace("addReservation: idReservation={}, guest={}, arrivalDate={}, departureDate={}, room={}, breakfastIncluded={}", idReservation, guest, arrivalDate, departureDate, room, breakfastIncluded);

        Reservation reservation = reservationRepository.save(new Reservation(idReservation, guest, arrivalDate, departureDate, room, breakfastIncluded));

        log.trace("addReservation: reservation={}", reservation);

        return reservation;
    }

    @Override
    public void delete(Long idReservation) {
        reservationRepository.deleteById(idReservation);
    }

}
