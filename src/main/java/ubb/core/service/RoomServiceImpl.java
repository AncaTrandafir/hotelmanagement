package ubb.core.service;

import org.springframework.beans.factory.annotation.Qualifier;
import ubb.core.model.Hotel;
import ubb.core.model.Reservation;
import ubb.core.model.Room;
import ubb.core.model.RoomType;
import ubb.core.repository.RoomRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service("roomService")
public class RoomServiceImpl implements RoomService {
    private static final Logger log = LoggerFactory.getLogger(ReservationServiceImpl.class);

    @Autowired
    @Qualifier("roomRepository")
    private RoomRepository roomRepository;

    @Override
    public List<Room> findAll() {
        log.trace("findAll --- method entered");

        List<Room> rooms = roomRepository.findAll();

        log.trace("findAll: rooms={}", rooms);

        return rooms;
    }

    @Override
    @Transactional
    public Room updateRoom(Long idRoom, Hotel hotel, RoomType roomType, int beds, float costPerNight) { //, Set<Reservation> reservations) {

        Optional<Room> room = roomRepository.findById(idRoom);

        room.ifPresent(s->{
            s.setIdRoom(idRoom);
            s.setHotel(hotel);
            s.setRoomType(roomType);
            s.setBeds(beds);
            s.setCostPerNight(costPerNight);
          //  s.setReservations(reservations);
        });

        return room.orElse(null);
    }

    @Override
    public Room addRoom(Long idRoom, Hotel hotel, RoomType roomType, int beds, float costPerNight) {       //, Set<Reservation> reservations) {

        Room room = (Room) roomRepository.save(new Room(idRoom, hotel, roomType, beds, costPerNight));  //, reservations));
        return room;
    }

    @Override
    public void delete(Long idRoom) {
        roomRepository.deleteById(idRoom);
    }


}
