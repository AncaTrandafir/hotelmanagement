package ubb.core.repository;


import org.springframework.stereotype.Repository;
//import ubb.core.model.BaseEntity;
import ubb.core.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Repository("roomRepository")
public interface RoomRepository // <T extends BaseEntity<ID>, ID extends Serializable>
        extends JpaRepository<Room, Long> {
}
