package ubb.core.repository;


import org.springframework.stereotype.Repository;
//import ubb.core.model.BaseEntity;
import ubb.core.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Repository("reservationRepository")
public interface ReservationRepository // <T extends BaseEntity<ID>, ID extends Serializable>
        extends JpaRepository<Reservation, Long> {
}
