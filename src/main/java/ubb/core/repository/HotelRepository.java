package ubb.core.repository;


import org.springframework.stereotype.Repository;
//import ubb.core.model.BaseEntity;
import ubb.core.model.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Repository("hotelRepository")
public interface HotelRepository // <T extends BaseEntity<ID>, ID extends Serializable>
        extends JpaRepository<Hotel, Long> {

    // jpa genereaza singur metode daca respect pattern de denumire
 // List<Hotel> findHotelByLocationContainingOrderByHotelNameHotelNameAsc(String string);
}
