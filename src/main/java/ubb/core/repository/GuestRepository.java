package ubb.core.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ubb.core.model.Guest;
import ubb.core.model.Room;

//import ubb.core.model.BaseEntity;

@Repository("guestRepository")
public interface GuestRepository // <T extends BaseEntity<ID>, ID extends Serializable>
        extends JpaRepository<Guest, Long> {
}
