package ubb.core.model;

public enum RoomType {
    economy,
    balcony,
    seaview,
    luxury
}
