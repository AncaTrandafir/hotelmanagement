package ubb.core.model;

import com.fasterxml.jackson.annotation.*;
import com.sun.xml.bind.v2.model.core.ID;
import lombok.*;

import javax.persistence.*;
import java.util.Date;



@Entity
@Table(name = "reservation")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@EqualsAndHashCode(callSuper = true)
//@ToString(callSuper = true)
@EqualsAndHashCode
@ToString

public class Reservation { //extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idreservation")
    private Long idReservation;

    //JoinTable
//    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
//    @JoinTable(
//            name = "reservation_guests",
//            joinColumns = @JoinColumn(name = "reservationid", referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "guestid", referencedColumnName = "id")
//    )
//    private Set<Guest> guests;

    @ManyToOne//(fetch = FetchType.LAZY)
    @JoinColumn(name = "guestid")
    @JsonIgnoreProperties({"hibernate_lazy_initializer", "handler"})
    @JsonManagedReference
    Guest guest;

    @Column(name = "arrivaldate")
    Date arrivalDate;

    @Column(name = "departuredate")
    Date departureDate;

    @ManyToOne    // mai multe rezervari la o camera
    @JoinColumn(name = "roomid")
  //  @JsonBackReference // to avoid infinite recursion
   // @JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@reservationId")
    @JsonManagedReference
    @JsonIgnoreProperties({"hibernate_lazy_initializer", "handler"})
    Room room;

    @Column(name = "breakfastincluded")
    boolean breakfastIncluded;



}


