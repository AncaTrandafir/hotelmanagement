package ubb.core.model;

import com.fasterxml.jackson.annotation.*;
import com.sun.xml.bind.v2.model.core.ID;
import lombok.*;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "hotel")
//@JsonIgnoreProperties({"rooms"})
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@EqualsAndHashCode(callSuper = true)
//@ToString(callSuper = true)
@EqualsAndHashCode
@ToString

public class Hotel { //extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idhotel")
    Long idHotel;

    @Size(min=3, message="Enter at least 3 characters.") // validare, binding se face in controller cu @Valid
    @Column(name = "hotelname")
    String hotelName;

    @Size(min=3, message="Enter at least 3 characters.")
    @Column(name = "location")
    String location;

    // Daca nu specific @joinTable, imi creeaza o tabela de legatura pt @OneToMany

//    @OneToMany(targetEntity=Room.class, cascade= CascadeType.ALL, fetch = FetchType.LAZY)
//    private Set<Room> rooms = new HashSet<>();  // un hotel are mai multe camere

//    @OneToMany
//    @JoinColumn(name = "idroom")
//    @JsonBackReference // to solve the infinite recursion
////    @JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@roomId")
//  //  @JsonIgnore
//  //  @JsonIgnoreProperties({"hibernate_lazy_initializer", "handler"})
//    Set<Room> rooms;






}
