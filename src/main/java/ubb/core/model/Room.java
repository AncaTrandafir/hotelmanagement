package ubb.core.model;

import com.fasterxml.jackson.annotation.*;
import com.sun.xml.bind.v2.model.core.ID;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "room")
//@JsonIgnoreProperties({"reservations"})
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@EqualsAndHashCode(callSuper = true)
//@ToString(callSuper = true)
@EqualsAndHashCode
@ToString

public class Room { //extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idroom")
    Long idRoom;

    @ManyToOne
    @JoinColumn(name = "hotelid")
//    @JsonBackReference // to avoid infinite recursion
//    @JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@hotelId")
//    @JsonIgnore
    @JsonManagedReference
    @JsonIgnoreProperties({"hibernate_lazy_initializer", "handler"})
    Hotel hotel;

    @Column(name = "roomtype")
    @Enumerated(EnumType.STRING)
    RoomType roomType;

    @Column(name = "beds")
    int beds;

    @Column(name = "costpernight")
    float costPerNight;

////    @OneToMany(targetEntity = Reservation.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @OneToMany
//    @JoinColumn(name = "idreservation")
//  //  @JsonManagedReference // to solve the infinite recursion
//    @JsonBackReference
//   // @JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@reservationId")
//    Set<Reservation> reservations;

}
