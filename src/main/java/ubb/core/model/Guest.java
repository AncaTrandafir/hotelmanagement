package ubb.core.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.xml.bind.v2.model.core.ID;
import lombok.*;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "guest")
//@JsonIgnoreProperties({"reservations"}) // ignora coloana de reservations pt ca am bi-directional association
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@EqualsAndHashCode(callSuper = true)
//@ToString(callSuper = true)
@EqualsAndHashCode
@ToString

public class Guest { // extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idguest")
    Long idGuest;

    @Column(name = "firstname")
    String firstName;

    @Column(name = "lastname")
    String lastName;

    @Column(name = "address")
    String address;

    @Column(name = "child")
    boolean child;

//   // @OneToMany(fetch = FetchType.LAZY, mappedBy = "guest") // owner-ul relatiei este guest; nu merge mappedBy
//    @OneToMany//(fetch = FetchType.LAZY)
//    @JoinColumn(name = "reservationid")
//    @JsonBackReference
//    Set<Reservation> reservations;


}
