// Nu mai am baseEntity, fac specific pt firecare entitate sa pot denumi id


//package ubb.hotelMngWeb.converter;
//
//import org.springframework.ui.Model;
//import ubb.core.model.BaseEntity;
//import ubb.hotelMngWeb.dto.BaseDto;
//
//import java.util.Collection;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//
//public abstract class BaseConverter // <Model extends BaseEntity<Long>, Dto extends BaseDto>
// implements Converter<Model, Dto> {
//
//    public Set<Long> convertModelsToIDs(Set<Model> models) {
//        return models.stream()
//                .map(model -> model.getId())
//                .collect(Collectors.toSet());
//    }
//
//    public Set<Long> convertDTOsToIDs(Set<Dto> dtos) {
//        return dtos.stream()
//                .map(dto -> dto.getId())
//                .collect(Collectors.toSet());
//    }
//
//    public Set<Dto> convertModelsToDtos(Collection<Model> models) {
//        return models.stream()
//                .map(model -> convertModelToDto(model))
//                .collect(Collectors.toSet());
//    }
//}
