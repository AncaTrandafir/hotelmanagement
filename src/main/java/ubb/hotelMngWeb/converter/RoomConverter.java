package ubb.hotelMngWeb.converter;

import ubb.core.model.Reservation;
import ubb.core.model.Room;
import ubb.hotelMngWeb.dto.RoomDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class RoomConverter { // extends BaseConverter<Room, RoomDto> {

    private static final Logger log = LoggerFactory.getLogger(RoomConverter.class);

   // @Override
    public Room convertDtoToModel(RoomDto dto) {
        Room room = new Room(dto.getIdRoom(), dto.getHotel(), dto.getRoomType(), dto.getBeds(), dto.getCostPerNight()); //, dto.getReservations());
       // room.setId(dto.getId());
        return room;
    }

   // @Override
    public RoomDto convertModelToDto(Room room) {
        RoomDto roomDto = new RoomDto(room.getIdRoom(), room.getHotel(), room.getRoomType(), room.getBeds(), room.getCostPerNight()); //, room.getReservations());
       // roomDto.setId(room.getId());
        return roomDto;
    }


//        public Set<Long> convertModelsToIDs(Set<Room> rooms) {
//        return rooms.stream()
//                .map(room -> room.getIdRoom())
//                .collect(Collectors.toSet());
//    }
//
//    public Set<Long> convertDTOsToIDs(Set<RoomDto> dtos) {
//        return dtos.stream()
//                .map(roomDto -> roomDto.getIdRoom())
//                .collect(Collectors.toSet());
//    }

    // din BaseConverter, adaptata pt fiecare cls in parte pt ca nu mai am clasa generala
    public Set<RoomDto> convertModelsToDtos(Collection<Room> rooms) {
        return rooms.stream()
                .map(room -> convertModelToDto(room))
                .collect(Collectors.toSet());
    }
}
