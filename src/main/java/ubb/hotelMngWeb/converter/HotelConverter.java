package ubb.hotelMngWeb.converter;

import ubb.core.model.Hotel;
import ubb.core.model.Room;
import ubb.hotelMngWeb.dto.HotelDto;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class HotelConverter { // extends BaseConverter<Hotel, HotelDto> {

   // @Override
    public Hotel convertDtoToModel(HotelDto dto) {
        Hotel hotel = new Hotel(dto.getIdHotel(), dto.getHotelName(), dto.getLocation()); //, dto.getRooms());
        return hotel;
    }

  //  @Override
    public HotelDto convertModelToDto(Hotel hotel) {
        HotelDto dto = new HotelDto(hotel.getIdHotel(), hotel.getHotelName(), hotel.getLocation()); //, hotel.getRooms());
        return dto;
    }

    // din BaseConverter, adaptata pt fiecare cls in parte pt ca nu mai am clasa generala
    public Set<HotelDto> convertModelsToDtos(Collection<Hotel> hotels) {
        return hotels.stream()
                .map(hotel -> convertModelToDto(hotel))
                .collect(Collectors.toSet());
    }
}
