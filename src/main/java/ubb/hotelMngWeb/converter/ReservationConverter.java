package ubb.hotelMngWeb.converter;

import ubb.core.model.Guest;
import ubb.core.model.Reservation;
import ubb.hotelMngWeb.dto.HotelDto;
import ubb.hotelMngWeb.dto.ReservationDto;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class ReservationConverter { //extends BaseConverter<Reservation, ReservationDto> {
   // @Override
    public Reservation convertDtoToModel(ReservationDto dto) {
        Reservation reservation = new Reservation(dto.getIdReservation(), dto.getGuest(), dto.getArrivalDate(), dto.getDepartureDate(), dto.getRoom(), dto.isBreakfastIncluded());
       // reservation.setId(dto.getId());
        return reservation;
    }

   // @Override
    public ReservationDto convertModelToDto(Reservation reservation) {
        ReservationDto dto = new ReservationDto(reservation.getIdReservation(), reservation.getGuest(), reservation.getArrivalDate(), reservation.getDepartureDate(), reservation.getRoom(), reservation.isBreakfastIncluded());
      //  dto.setId(reservation.getId());
        return dto;
    }

    // din BaseConverter, adaptata pt fiecare cls in parte pt ca nu mai am clasa generala
    public Set<ReservationDto> convertModelsToDtos(Collection<Reservation> reservations) {
        return reservations.stream()
                .map(reservation -> convertModelToDto(reservation))
                .collect(Collectors.toSet());
    }
}
