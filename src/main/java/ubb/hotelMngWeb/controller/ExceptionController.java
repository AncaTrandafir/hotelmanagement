package ubb.hotelMngWeb.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

// Creez o pagina custom pt erori sa nu-mi mai afiseze by default WhiteLabelPage

@Controller("error")
public class ExceptionController {

    private static final Logger log = LoggerFactory.getLogger(ExceptionController.class);       // logam exceptiile

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {     // ModelAndView = Model + view
        log.error("Request: " + req.getRequestURL() + " raised " + ex);

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", ex);                 // exceptia
        mav.addObject("url", req.getRequestURL());      // url care a declansat-o
        mav.setViewName("error");                                    // directionare catre view "error"
        return mav;
    }

}
