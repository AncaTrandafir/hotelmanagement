

package ubb.hotelMngWeb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
// import ubb.core.service.LoginService;

@Controller
@SessionAttributes("name")
public class WelcomeController {

//    @Autowired            aveam nevoie de service cand nu aveam Security
//    LoginService loginService;

    @RequestMapping("/")
    public String showWelcomePage(ModelMap model){
        model.put("name", getLoggedInUserName(model));       // mapez un model: la sessionAttribute "name" pun ce imi returneaza metoda de mai jos: getUsername din formularul de login
        return "welcome";   // orice url acceseaza il redirectionez catre pagina de welcome, nu poate tasta direct /hotels atc
    }


    // Spring Security
    private String getLoggedInUserName(ModelMap model) {        // the one who's logged in is called a Principal
        Object principal =
                SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();        // luam un Principal din cls SecurityContextHolder cu met statica getContext.... etc
                                                            // asignam Principal unui Object

        if (principal instanceof UserDetails)   // UserDetails - stores users details pe care le introduce in formularul de login
            return ((UserDetails) principal).getUsername();     // facem cast de UserDetails si luam cu met get.Username()

        return principal.toString();
    }



    // Model
//    @RequestMapping("/login")
//    public String welcome(@RequestParam String name, ModelMap model){
//        model.put("name", name);        // cream o variabila name pe care o punem in model si model o transmite mai departe catre view care o afiseaza
//        return "login";
//    }


    // Cand nu aveam Spring Security


//    @GetMapping("/login")
//    public String login(ModelMap model){
//        return "login";
//    }
//
//    @PostMapping("/login")  // user si pass cu postMethod ca sa nu apara in URL
//    public String welcome(@RequestParam String name, String password, ModelMap model){
//
//        boolean isValidUser = loginService.validateUser(name, password);
//
//        if (!isValidUser) {
//            model.put("errorMessage", "Invalid Credentials");
//            return "login";
//        }
//
//        model.put("name", name);
//        return "welcome";
//    }

}
