package ubb.hotelMngWeb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import ubb.core.model.Hotel;
import ubb.core.service.HotelService;
import org.springframework.beans.factory.annotation.Qualifier;
import ubb.hotelMngWeb.converter.HotelConverter;
import ubb.hotelMngWeb.dto.HotelDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@Controller
//@RestController("hotelController")    // @RestController returneaza json si este viewless; sa imi apara view cu model simplu @Controller
@RequestMapping//(value = "/api")
public class HotelController {

    private static final Logger log = LoggerFactory.getLogger(HotelController.class);

    @Autowired
    @Qualifier("hotelService")
    private HotelService hotelService;

    @Autowired
    private HotelConverter hotelConverter;



    @GetMapping("/hotels")
  //  @RequestMapping(value = "/hotels", method = RequestMethod.GET)
    public List<HotelDto> getHotels(ModelMap model) {
        log.trace("getHotels");

        List<Hotel> hotels = hotelService.findAll();
        model.put("hotels", hotelService.findAll()); // creez un model de tip (cheie, val), in care la string hotels returnez service.findAll;

        log.trace("getHotels: hotels={}", hotels);

        return new ArrayList<>(hotelConverter.convertModelsToDtos(hotels));
    }





    /**
     * This creates a new address object for and stuffs it into the model
     */
    @ModelAttribute("hotel")
    // aici hotel este CommandBean pe care l-am declarat in html; construim o instanta cu valori random pt ca se vor suprascrie sau o instanta goala;
    // la fel procedasem si cand folosisem @modelAttribute; prefer CommandBean ca sa pot introduce validarile entitatilor din controller in form din html, folosind CommandBean si tag-uri speciale ale Spring, pe care le injectez cu prima linie din html
    // creez o instanta pt attributeName "hotel"
    public Hotel hotelModel(ModelMap model) {
        Hotel hotel = new Hotel();
        model.addAttribute("hotel", hotel);
        return hotel;
    }


    @GetMapping("/addHotel") // cerere tip Get sa-mi returnetze addHotel.jsp
    public String showAddHotelPage(ModelMap model) {
        return "addHotel";  // acceseaza addHotel.jsp
    }


    @PostMapping("/addHotel")
  //  @RequestMapping(value = "/hotels", method = RequestMethod.POST)
    public String addHotel(ModelMap model,
            //@RequestBody final HotelDto hotelDto) // cand fac @RequestBody cer sub format Json; imi treb @requestParam sa dau fircare parametru tip String
//                            @RequestParam(value="idHotel") Long idHotel,
//                            @RequestParam(value="hotelName") String hotelName,
//                           @RequestParam(value="location") String location ){
//
            // nu pot folosi mai multi@RequestParam si sa-i transmit in url, ci folosesc @ModelAttribute sau CommandBean
//
            @Valid Hotel hotel,     // un model care imbraca un RequestBody sub forma de view; aici Hotel este CommandBean pe care l-am initializat mai sus in showAhhHotelPage
            BindingResult resultBinding) {      // sau cand foloseam @ModelAttribute era un model default creat in @ModelAttribute("hotel")

        hotelModel(model); // injectez o un CommandBean care este un view pt hotel; are niste valori random care se suprascriu
        // sau model.put("hotel", hotel);

        if (resultBinding.hasErrors()) { //BindingResult e bean pt validare si merge doar cu CommandBean, nu si cu modelAttribute
            return "addHotel"; // eroare-> se intoarce la formular de add
        }


        model.addAttribute("hotelId", 100);  // nu conteaza val pt id ca se suprascrie pt ca se autoincrementeaza
        model.addAttribute("hotelName", hotel.getHotelName()); // adaug atribut, este path din html form,, apoi il iau cu get si il bag in variabila;
        model.addAttribute("location", hotel.getLocation());


//      Cand aveam @RequestParam

//        log.trace("addHotel: hotelDto={}", hotelDto);
//
//        Hotel hotel = hotelService.addHotel(
//                hotelDto.getIdHotel(),
//                hotelDto.getHotelName(),
//                hotelDto.getLocation());
//           //     hotelDto.getRooms());

      //  Hotel hotelModel = hotelService.addHotel(idHotel, nameHotel, location);
      //  HotelDto result = hotelConverter.convertModelToDto(hotel);

        log.trace("addHotel: hotel={}", hotel);

        Long idHotel = (Long)model.get("idHotel");
        System.out.println("idHotel "+ idHotel);
        String hotelName = (String)model.get("hotelName");
        System.out.println("hotelname "+ hotelName);
        String location = (String)model.get("location");
        HotelDto hotelDto = new HotelDto(idHotel, hotelName, location);

        hotel = hotelService.addHotel(hotelDto.getIdHotel(), hotelDto.getHotelName(), hotelDto.getLocation());
        HotelDto result = hotelConverter.convertModelToDto(hotel);
       // model.put("hotels", result);

        log.trace("addHotel: result={}", result);

      //  model.addAttribute("hotel", result);

        return "redirect:/hotels";
    }





    @GetMapping("/delete-hotel") // nu mai transmit id in url, ci il declar @requestParam in numele metodei
//    @DeleteMapping("/delete-hotel/{id}")      // /{id} este @PathVariable
 //   @RequestMapping(value = "/hotels/{id}", method = RequestMethod.DELETE)
    public String deleteHotel(@RequestParam Long id) {
                        // (@PathVariable final Long id) { // pt jSon
        log.trace("deleteHotel: id={}", id);

        hotelService.delete(id);
        ResponseEntity<?> status = new ResponseEntity(HttpStatus.OK);
        return "redirect:/hotels"; // stergem hotel, redirectionam catre lista
    }




    @GetMapping("/updateHotel")
    //@PutMapping("/addHotel/{id}")
    // @RequestMapping(value = "/hotels/{id}", method = RequestMethod.PUT)
    public String updateHotel(@RequestParam Long id, Model model) {
        return "addHotel"; // iau id si ma duc pe addHotel
    }

    @PostMapping("/updateHotel")
    //@PutMapping("/addHotel/{id}")
    // @RequestMapping(value = "/hotels/{id}", method = RequestMethod.PUT)
    public String updateHotel(
                     @RequestParam Long id,
                   //  @PathVariable Long id    -> pt jSon
                     ModelMap model,
                     @Valid Hotel hotel,
                     BindingResult bindingResult ) {

//            @PathVariable final Long id,
//            @RequestBody final HotelDto hotelDto) {

        log.trace("updateHotel: id={}, hotel={}", id, hotel);
        System.out.println("ID = " + id);

        model.put("hotel", hotel); // injectez un CommandNean = view pt hotel
       // hotelModel(model);

        if (bindingResult.hasErrors())
            return "addHotel";

      //  model.addAttribute("hotelId", 100);
        model.addAttribute("hotelName", hotel.getHotelName()); // adaug atribut, este path din html form,, apoi il iau cu get si il bag in variabila;
        model.addAttribute("location", hotel.getLocation());

        String hotelName = (String)model.get("hotelName");
        String location = (String)model.get("location");

        hotel = hotelService.updateHotel(id, hotelName, location);
        HotelDto result = hotelConverter.convertModelToDto(hotel);

        log.trace("updateHotel: result={}", result);

        return "redirect:/hotels";
    }




    @RequestMapping(value = "/hotels/{id}", method = RequestMethod.GET)
    public Optional<Hotel> retrieveHotel(@PathVariable("id") Long id) {
        return hotelService.findById(id);
    }

}
