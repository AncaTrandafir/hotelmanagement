package ubb.hotelMngWeb.controller;

import ubb.core.model.Room;
import ubb.core.service.RoomService;
import org.springframework.beans.factory.annotation.Qualifier;
import ubb.hotelMngWeb.converter.RoomConverter;
import ubb.hotelMngWeb.dto.RoomDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController("roomController")
@RequestMapping(value = "/api")
public class RoomController {

    private static final Logger log = LoggerFactory.getLogger(RoomController.class);

    @Autowired
    @Qualifier("roomService")
    private RoomService roomService;

    @Autowired
    private RoomConverter roomConverter;


    @RequestMapping(value = "/rooms", method = RequestMethod.GET)
    public List<RoomDto> getRooms() {
        log.trace("getRooms");

        List<Room> rooms = roomService.findAll();

        log.trace("getRooms: rooms={}", rooms);

        return new ArrayList<>(roomConverter.convertModelsToDtos(rooms));
    }

    @RequestMapping(value = "/rooms/{id}", method = RequestMethod.PUT)
    public RoomDto updateRoom(
            @PathVariable final Long id,
            @RequestBody final RoomDto roomDto) {
        log.trace("updateRoom: id={}, roomDtoMap={}", id, roomDto);

        Room room = roomService.updateRoom(id,
                roomDto.getHotel(),
                roomDto.getRoomType(),
                roomDto.getBeds(),
                roomDto.getCostPerNight());
             //   roomDto.getReservations());

        RoomDto result = roomConverter.convertModelToDto(room);

        log.trace("updateRoom: result={}", result);

        return result;
    }

    @RequestMapping(value = "/rooms", method = RequestMethod.POST)
    public RoomDto addRoom(
            @RequestBody final RoomDto roomDto) {
        log.trace("addRoom: roomDto={}", roomDto);

        Room room = roomService.addRoom(
                roomDto.getIdRoom(),
                roomDto.getHotel(),
                roomDto.getRoomType(),
                roomDto.getBeds(),
                roomDto.getCostPerNight());
               // roomDto.getReservations());

        RoomDto result = roomConverter.convertModelToDto(room);

        log.trace("addRoom: result={}", result);

        return result;
    }

    @RequestMapping(value = "/rooms/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteRoom(@PathVariable final Long id) {
        log.trace("deleteRoom: idd={}", id);

        roomService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
