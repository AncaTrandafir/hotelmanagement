package ubb.hotelMngWeb.controller;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import ubb.core.model.Guest;
import ubb.core.model.Reservation;
import ubb.core.model.Room;
import ubb.core.service.ReservationService;
import org.springframework.beans.factory.annotation.Qualifier;
import ubb.hotelMngWeb.converter.ReservationConverter;
import ubb.hotelMngWeb.dto.ReservationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Controller("reservationController")
@RequestMapping
public class ReservationController {

    private static final Logger log = LoggerFactory.getLogger(ReservationController.class);

    @Autowired
    @Qualifier("reservationService")
    private ReservationService reservationService;

    @Autowired
    private ReservationConverter reservationConverter;



    @GetMapping("/reservations")
    public List<ReservationDto> getReservations(ModelMap model) {
        log.trace("getReservations");
        List<Reservation> reservations = reservationService.findAll();
        model.put("reservations", reservationService.findAll());
        log.trace("getReservations: reservations={}", reservations);

        return new ArrayList<>(reservationConverter.convertModelsToDtos(reservations));
    }


    // am nevoie de un InitBinder pt Date altfel am eroare la adaugare, unde ar trebui sa adaug text(String), iar data in obiect este LocalDate

    // Failed to convert property value of type java.lang.String to required type java.time.LocalDate for property arrivalDate;
    // nested exception is org.springframework.core.convert.ConversionFailedException: Failed to convert from type [java.lang.String] to
    // type [@javax.persistence.Column java.time.Date] for value 2020-10-19; nested exception is java.lang.IllegalArgumentException:
    // Parse attempt failed for value [2020-10-19]

    @InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}







    /**
     * This creates a new address object for and stuffs it into the model
     */
    @ModelAttribute("reservation")
    public Reservation reservationModel(ModelMap model) {
        Reservation reservation = new Reservation();
        model.addAttribute("reservation", reservation);
        return reservation;
    }


    @GetMapping("/addReservation")
    public String showAddReservationPage(ModelMap model) {
        return "addReservation";
    }


    @PostMapping("/addReservation")
    public String addReservation(ModelMap model,
                           @Valid Reservation reservation,
                           BindingResult resultBinding) {

        log.trace("addReservation: reservation={}", reservation);

        model.put("reservation", reservation);

        if (resultBinding.hasErrors()) {
            return "addReservation";
        }

        model.addAttribute("idReservation", 100);
        model.addAttribute("guest", reservation.getGuest());
        model.addAttribute("arrivalDate", reservation.getArrivalDate());
        model.addAttribute("departureDate", reservation.getDepartureDate());
        model.addAttribute("room", reservation.getRoom());
        model.addAttribute("breakfastIncluded", reservation.isBreakfastIncluded());

        Long idReservation = (Long)model.get("idReservation");
        Guest guest = (Guest)model.get("guest");
        Date arrivalDate = (Date)model.get("arrivalDate");
        Date departureDate = (Date)model.get("departureDate");
        Room room = (Room)model.get("room");
        Boolean breakfastIncluded = (Boolean)model.get("breakfastIncluded");

        reservation = reservationService.addReservation(idReservation, guest, arrivalDate, departureDate, room, breakfastIncluded);
        ReservationDto result = reservationConverter.convertModelToDto(reservation);

        log.trace("addHotel: result={}", result);

        return "redirect:/reservations";
    }



    @GetMapping("/delete-reservation")
    public String deleteReservation(@RequestParam Long id) {
        log.trace("deleteHotel: id={}", id);
        reservationService.delete(id);
        return "redirect:/reservations";
    }



    @GetMapping("/updateReservation")
    public String updateReservation(@RequestParam Long id, Model model) {
        return "addReservation";
    }



    @PostMapping("/updateReservation")
    public String updateHotel( @RequestParam Long id,
                             ModelMap model,
                             @Valid Reservation reservation,
                             BindingResult bindingResult ) {

        log.trace("updateReservation: id={}, reservation={}", id, reservation);

        model.put("reservation", reservation);

        if (bindingResult.hasErrors())
            return "addReservation";

        model.addAttribute("guest", reservation.getGuest());
        model.addAttribute("arrivalDate", reservation.getArrivalDate());
        model.addAttribute("departureDate", reservation.getDepartureDate());
        model.addAttribute("room", reservation.getRoom());
        model.addAttribute("breakfastIncluded", reservation.isBreakfastIncluded());

        Long idReservation = (Long)model.get("idReservation");
        Guest guest = (Guest)model.get("guest");
        Date arrivalDate = (Date)model.get("arrivalDate");
        Date departureDate = (Date)model.get("departureDate");
        Room room = (Room)model.get("room");
        Boolean breakfastIncluded = (Boolean)model.get("breakfastIncluded");

        reservation = reservationService.updateReservation(idReservation, guest, arrivalDate, departureDate, room, breakfastIncluded);
        ReservationDto result = reservationConverter.convertModelToDto(reservation);

        log.trace("updateReservation: result={}", result);

        return "redirect:/reservations";
    }









    // Json

//    @RequestMapping(value = "/reservations", method = RequestMethod.GET)
//    public List<ReservationDto> getReservations() {
//        log.trace("getReservations");
//
//        List<Reservation> reservations= reservationService.findAll();
//
//        log.trace("getReservations: reservations={}", reservations);
//
//        return new ArrayList<>(reservationConverter.convertModelsToDtos(reservations));
//    }
//
//
//    @RequestMapping(value = "/reservations/{id}", method = RequestMethod.PUT)
//    public ReservationDto updateReservation(
//            @PathVariable final Long id,
//            @RequestBody final ReservationDto reservationDto) {
//        log.trace("updateReservation: id={}, reservationDtoMap={}", id, reservationDto);
//
//        Reservation reservation = reservationService.updateReservation(id,
//                reservationDto.getGuest(),
//                reservationDto.getArrivalDate(),
//                reservationDto.getDepartureDate(),
//                reservationDto.getRoom(),
//                reservationDto.isBreakfastIncluded());
//
//        ReservationDto result = reservationConverter.convertModelToDto(reservation);
//
//        log.trace("updateReservation: result={}", result);
//
//        return result;
//    }
//
//    @RequestMapping(value = "/reservations", method = RequestMethod.POST)
//    public ReservationDto addReservation(
//            @RequestBody final ReservationDto reservationDto) {
//        log.trace("addReservation: reservationDto={}", reservationDto);
//
//        Reservation reservation = reservationService.addReservation(
//                reservationDto.getIdReservation(),
//                reservationDto.getGuest(),
//                reservationDto.getArrivalDate(),
//                reservationDto.getDepartureDate(),
//                reservationDto.getRoom(),
//                reservationDto.isBreakfastIncluded());
//
//        ReservationDto result = reservationConverter.convertModelToDto(reservation);
//
//        log.trace("addreservation: result={}", result);
//
//        return result;
//    }
//
//    @RequestMapping(value = "/reservations/{id}", method = RequestMethod.DELETE)
//    public ResponseEntity<?> deleteReservation(@PathVariable final Long id) {
//        log.trace("delateReservation: id={}", id);
//
//        reservationService.delete(id);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
}
