package ubb.hotelMngWeb.dto;

import ubb.core.model.Hotel;
import lombok.*;
import ubb.core.model.Reservation;
import ubb.core.model.RoomType;

import java.util.Set;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class RoomDto { // extends BaseDto {
    Long idRoom;
    Hotel hotel;
    RoomType roomType;
    int beds;
    float costPerNight;
   // Set<Reservation> reservations;
}
