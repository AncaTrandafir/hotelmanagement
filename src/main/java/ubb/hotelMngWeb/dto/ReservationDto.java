package ubb.hotelMngWeb.dto;

import lombok.*;
import ubb.core.model.Guest;
import ubb.core.model.Room;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class ReservationDto { // extends BaseDto {
    Long idReservation;
    Guest guest;
    Date arrivalDate;
    Date departureDate;
    Room room;
    boolean breakfastIncluded;


}
