package ubb.hotelMngWeb.dto;

import ubb.core.model.Reservation;
import ubb.core.model.Room;
import lombok.*;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class HotelDto { //extends BaseDto {
    Long idHotel;
    String hotelName;
    String location;
  //  Set<Room> rooms;

}
