<!-- in footer inlcud linkurile de javaScript -->

<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script src="webjars/bootstrap-datepicker/1.0.1/js/bootstrap-datepicker.js"></script>

<script>                               // javaScript pt datePicker
$('#arrivalDate').datepicker({
    format : 'dd/mm/yyyy'
});

$('#departureDate').datepicker({  // face identificare dupa id cu #
    format : 'dd/mm/yyyy'
});
</script>

</body>

</html>