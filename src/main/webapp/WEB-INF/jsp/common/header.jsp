<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  <!-- formatare -->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    <!-- SpringForm -->

<html>

<head>
    <title>Hotel Management</title>
    <link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
          rel="stylesheet">
    <link href="../../../../resources/static/css/custom.css"
          rel="stylesheet">

</head>

<body>

