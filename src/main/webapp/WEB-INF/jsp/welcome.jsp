<%@ include file="common/header.jsp" %>
<%@ include file="common/navigation.jsp" %>

<div class="container">
    Welcome ${name}!
    See all hotels: <a class="btn btn-success" href="http://localhost:8080/hotels"> Click here </a>
    See all reservations: <a class="btn btn-success" href="http://localhost:8080/reservations"> Click here </a>
</div>

<%@ include file="common/footer.jsp" %>