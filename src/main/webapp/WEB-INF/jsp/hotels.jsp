<%@ include file="common/header.jsp" %>
<%@ include file="common/navigation.jsp" %>

<div class="container">
    <table class="table table-striped">
        <caption>Hotels across the country</caption>
        <thead>
        <tr>
            <th>Id</th>
            <th>Hotel name</th>
            <th>Location</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${hotels}" var="hotel">
            <tr>
                <td>${hotel.idHotel}</td>
                <td>${hotel.hotelName}</td>
                <td>${hotel.location}</td>
                <td><a type="button" class="btn btn-warning" href="http://localhost:8080/updateHotel?id=${hotel.idHotel}">Edit</a></td>
                <td><a type="button" class="btn btn-warning" href="http://localhost:8080/delete-hotel?id=${hotel.idHotel}">Delete</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div> <a class="button" href="http://localhost:8080/addHotel">Add a Hotel</a></div>


</div>

<%@ include file="common/footer.jsp" %>

