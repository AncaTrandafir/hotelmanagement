<%@ include file="common/header.jsp" %>
<%@ include file="common/navigation.jsp" %>

<div class="container">

    Add reservation for ${name}

    <form:form method="post" attributeName="reservation" modelAttribute="reservation">
        <form:hidden path="idReservation"/>
        <fieldset class="form-group">

            <form:label path="guest">Guest ID: </form:label>
            <form:input path="guest"  type="text" class="form-control" required="required"/>
            <form:errors path="guest" cssClass="text-warning"/>

            <form:label path="arrivalDate">Arrival date: </form:label>
            <form:input path="arrivalDate"  type="text" class="form-control" required="required"/>
            <form:errors path="arrivalDate" cssClass="formFieldError"/>

            <form:label path="departureDate">Departure date: </form:label>
            <form:input path="departureDate"  type="text" class="form-control" required="required"/>
            <form:errors path="departureDate" cssClass="formFieldError"/>

            <form:label path="room">Room ID: </form:label>
            <form:input path="room"  type="text" class="form-control" required="required"/>
            <form:errors path="room" cssClass="formFieldError"/>

            <form:label path="breakfastIncluded">Breakfast included: </form:label>
            <form:input path="breakfastIncluded"  type="text" class="form-control" required="required"/>
            <form:errors path="breakfastIncluded" cssClass="formFieldError"/>

        </fieldset>
            <button type="submit" class="btn btn-success">Add reservation</button>
    </form:form>

<%@ include file="common/footer.jsp" %>