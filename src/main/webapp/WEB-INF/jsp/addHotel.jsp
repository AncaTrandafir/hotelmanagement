<%@ include file="common/header.jsp" %>
<%@ include file="common/navigation.jsp" %>

<div class="container">

    Add hotel for ${name}

    <form:form method="post" attributeName="hotel" modelAttribute="hotel"  >  <!-- hotel este CommandBean pe care il specific in Controller la showAddHotelPage, acolo declar attributeName("hotel") -->

        <form:hidden path="idHotel"/>
        <fieldset class="form-group">

            <form:label path="hotelName">Hotel name: </form:label>   <!-- tag-uri speciale ale lui SpringBoot, pe care le-am importaat cu prima linie; form: si treb sa specific path care face referire la atributul obiectlui -->
            <form:input path="hotelName"  type="text" class="form-control" required="required"/>    <!-- path descrie atributul lui hotel, care este modelAttribute/CommandName -->
            <form:errors path="hotelName" cssClass="text-warning"/>

            <form:label path="location">Location: </form:label>
            <form:input path="location"  type="text" class="form-control" required="required"/>
            <form:errors path="location" cssClass="formFieldError"/>  <!-- Nu-mi afiseaza erorile, nu stiu DE CE-->

        </fieldset>
            <button type="submit" class="btn btn-success">Add hotel</button>
    </form:form>
</div>

<%@ include file="common/footer.jsp" %>

