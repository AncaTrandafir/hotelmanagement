<%@ include file="common/header.jsp" %>
<%@ include file="common/navigation.jsp" %>

<div class="container">
    <table class="table table-striped">
        <caption>Reservations</caption>
        <thead>
        <tr>
            <th>ID</th>
            <th>Guest</th>
            <th>Arrival date</th>
            <th>Departure date</th>
            <th>Room</th>
            <th>Breakfast included</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${reservations}" var="reservation">
            <tr>
                <td>${reservation.idReservation}</td>
                <td>${reservation.guest}</td>
                <td><fmt:formatDate value="${reservation.arrivalDate}" pattern = "dd.MM.yyyy"/></td>
                <<td><fmt:formatDate value="${reservation.departureDate}" pattern = "dd.MM.yyyy"/></td>
                <td>${reservation.room}</td>
                <td>${reservation.breakfastIncluded}</td>
                <td><a type="button" class="btn btn-warning" href="http://localhost:8080/updateReservation?id=${reservation.idReservation}">Edit</a></td>
                <td><a type="button" class="btn btn-warning" href="http://localhost:8080/delete-hotel?id=${reservation.idReservation}">Delete</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div> <a class="button" href="http://localhost:8080/addReservation">Add a Reservation</a></div>

</div>

<%@ include file="common/footer.jsp" %>


