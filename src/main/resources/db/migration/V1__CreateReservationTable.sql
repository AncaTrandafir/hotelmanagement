insert into guest
values (1, 'Galati, Mercurului 47', false, 'Ene', 'Georgescu')

insert into hotel
values (1, 'Belvedere', 'Cluj-Napoca'),
(2, 'Traian', 'Braila')

insert into room
values (1, 2, 250, 'economy', 1),
(2, 2, 300, 'luxury', 1),
(3, 2, 270, 'balcony', 1),
(4, 2, 250, 'economy', 2),
(5, 2, 300, 'luxury', 2),
(6, 2, 270, 'balcony', 2)

insert into guest values
(2, 'Galati, Sirenei 39', false, 'Geo', 'Popa')

insert into reservation values
(1, '2020-07-19', true, '2020-07-20', 1, 1),
(2, '2020-07-22', true, '2020-07-27', 2, 6),
(3, '2020-08-12', false, '2020-08-20', 2, 4)