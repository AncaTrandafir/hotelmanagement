package ubb.hotelMngWeb.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@ComponentScan("ubb.hotelMngWeb.Controller")
@ExtendWith(SpringExtension.class)
class HotelControllerTestIT {

    @Test
    void getHotels() {
    }

    @Test
    void hotelModel() {
    }

    @Test
    void showAddHotelPage() {
    }

    @Test
    void addHotel() {
    }

    @Test
    void deleteHotel() {
    }

    @Test
    void updateHotel() {
    }

    @Test
    void testUpdateHotel() {
    }

    @Test
    void retrieveHotel() {
    }
}