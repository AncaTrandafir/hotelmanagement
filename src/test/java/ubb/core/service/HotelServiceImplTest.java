package ubb.core.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import ubb.core.model.Hotel;

import java.util.stream.LongStream;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@Transactional
@ComponentScan("ubb.core.service")
@ExtendWith(SpringExtension.class)
class HotelServiceImplTest {

    @Autowired
    @Qualifier("hotelService")
    private HotelService hotelService;


    @Test
    void findAll() {
//        List<String> dbNames = hotelService.findAll().stream()
//                .map(Hotel::getHotelName)
//                .collect(Collectors.toList());
//        assertThat(dbNames, containsInAnyOrder("Belvedere", "Traian"));

    }

    @Test
    void updateHotel() {
        Hotel hotel = hotelService.addHotel(1L, "Belvedere", "Cluj Napoca");
        String newName = "Clujana";
        hotelService.updateHotel(1L, newName, "Cluj Napoca");
        assertEquals(newName, hotel.getHotelName());
    }

    @Test
    void addHotel() {
        Hotel hotel = hotelService.addHotel(5L, "Belvedere", "Cluj Napoca");
        assertNotEquals(0, hotelService.findAll().size());
    }

    @Test
    void delete() {
        hotelService.addHotel(1L, "Belvedere", "Cluj Napoca");
        hotelService.addHotel(2L, "Belvedere", "Cluj Napoca");
        hotelService.addHotel(3L, "Belvedere", "Cluj Napoca");
        LongStream.rangeClosed(1, 3)
                .forEach(id -> {
                    hotelService.delete(id);
                });
        assertEquals(0, hotelService.findAll().size());
    }
}